import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class parabola{
    public static void main(String [] args) throws IOException{
	FileInputStream in = null;
	try{
            in = new FileInputStream("input_parabola.txt");
            System.out.println("-------------------------");
            System.out.println("Parabola");
            int a=0,b=0,c=0;
            String string="";
            int reader;
            boolean negative=false;
            do{
                reader=(char)in.read();
		if (reader==45)negative=true;
		else if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
                    throw new IOException();
		}
		if (reader ==' ')break;
		if(reader!=45)string=string+String.valueOf((char)reader);
            }
            while(true);
            a =Integer.valueOf(string);
            if(negative)a=-a;
            negative=false;
            string="";
            do{
                reader=(char)in.read();
		if (reader==45)negative=true;
                else if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
                    throw new IOException();
		}
		if (reader ==' ')break;
		if(reader!=45)string=string+String.valueOf((char)reader);
            }
            while(true);
            b =Integer.valueOf(string);
            if(negative)b=-b;
            negative=false;
            string="";
            if ((reader=in.read())==(-1))System.out.println("Akhir dari file");
            while(true){
                if (reader==45)negative=true;
                else if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
                    throw new IOException();
                }
            if(reader!=45)string=string+String.valueOf((char)reader);
            if ((reader=in.read())==-1)break;
            else continue;
            }
            c =Integer.valueOf(string);
            if(negative)c=-c;
            negative=false;
            int D=(b*b) - (4*a*c);
            int titikBalikX=(-b/(2*a));
            int titikBalikY= D / (-4*a);
            System.out.println(a+"x^2 + "+b+"x + "+c);
            System.out.println("titik balik ("+titikBalikX+","+titikBalikY+")");
            if (a>0){
                System.out.println("Grafik Terbuka Ke Atas, Titik Balik Minimum");
            }
            else if (a<0){
                System.out.println("Grafik Terbuka Ke Bawah, Titik Balik Maksimum");
            }
            if (titikBalikX < 0) System.out.println("Titik Balik Terletak Di kiri Sumbu Y");
            else if (titikBalikX > 0) System.out.println("Titik Balik Terletak Di kanan Sumbu Y");
            else System.out.println("Titik Balik Terletak Di Sumbu Y");
            if (c < 0) System.out.println("Grafik Memotong Sumbu Y Di bawah Sumbu X");
            else if (c > 0) System.out.println("Grafik Memotong Sumbu Y Di atas Sumbu X");
            else System.out.println("Grafik Memotong Sumbu Y Sumbu X");
            System.out.println("-------------------------");
	}
        catch(FileNotFoundException e){
            System.out.println("\nFile not found");
            System.out.println("Proogram ini memerlukan file input_parabola.txt (dengan 3 nilai didalamnya) untuk dijalankan");
            System.out.println("-------------------------");
	}
        catch(IOException e){
            System.out.println("Error.");
            System.out.println("The input_parabola.txt doesn't contain 3 values (separated by a space)");
            System.out.println("-------------------------");
	}
        catch(NumberFormatException e){
            System.out.println("Error.");
            System.out.println("File input_parabola.txt Tidak memiliki 3 nilai (dipisahkan dengan space)");
            System.out.println("-------------------------");
	}
        finally{
            if (in != null) {
            in.close();
            }
        }
    }
}
