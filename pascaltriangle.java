import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;
public class pascaltriangle{
    public static void main(String [] args) throws IOException{
        FileInputStream in = null;
        FileOutputStream out = null;

        try{
            in = new FileInputStream("pascalInput.txt");
            out = new FileOutputStream("pascalOutput.txt");
            int rows;
            int reader;
            String string="";
            while((reader=in.read()) !=-1){
                if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
                    throw new IOException();
                }
                string=string+String.valueOf((char)reader);
                if((reader=in.read()) !=-1){                   
                    string=string+String.valueOf((char)reader);
                }
            }
            rows =Integer.valueOf(string);
            String newLine = System.getProperty("line.separator");
            int c_row,c_number;
                for (c_row = 0; c_row < rows; c_row++) {
                    int spaces =(rows-c_row);
                    for (int x=0; x<spaces;x++){
                      string="   ";
                        for(int index=0;index<string.length();index++){
                          out.write(string.charAt(index));
                        }
                    }
                    long number=1;
                    for (c_number = 0; c_number <= c_row; c_number++){
                        if(number<10){
                            string="  "+number+"   ";
                            for(int index=0;index<string.length();index++){
                            out.write(string.charAt(index));
                            }
                        }else if(number<100){
                            string=("  "+number+"  ");
                            for(int index=0;index<string.length();index++){
                            out.write(string.charAt(index));
                            }
                        }
                        else if(number<1000){
                            string=(" "+number+"  ");
                            for(int index=0;index<string.length();index++){
                            out.write(string.charAt(index));
                            }
                        }
                        else{
                            string=(" "+number+" ");
                            for(int index=0;index<string.length();index++){
                            out.write(string.charAt(index));
                            }
                        }
                        number=number*(c_row - c_number) / (c_number + 1);	
                    }
                    string=newLine;
                    for(int index=0;index<string.length();index++){
                        out.write(string.charAt(index));
                    }
               }
                System.out.println("pascalOutput.txt telah dibuat");
        }
        catch(FileNotFoundException e){
            System.out.println("\nFile tidak ditemukan");
            System.out.println("pascalInput.txt tidak ditemukan");
            System.out.println("-------------------------");
        }
        catch(IOException e){
            System.out.println("Error.");
            System.out.println("File pascalInput.txt tidak memiliki nilai");
            System.out.println("Silahkan ganti nilai pada file pascalInput.txt");
            System.out.println("-------------------------");
        }
        catch(NumberFormatException e){
            System.out.println("Error.");
            System.out.println("File pascalInput.txt tidak memiliki nilai");
            System.out.println("Silahkan ganti nilai pada file pascalInput.txt");
            System.out.println("-------------------------");
        }
        finally{
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        }
    }
}
